/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package characters;

import static java.lang.Math.floor;
import javafx.geometry.Rectangle2D;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;


/**
 *
 * @author yianqian
 */
public class Characters {
    //
    private int imgWidth;
    private int imgHeight;
    //
    private int viewportWidth;
    private int viewportHeight;
    private int viewportNum;
    //private int viewportIndex;

    
    private double x_centre;
    private double y_centre;
    
    private Boolean isVisible;
    
    
    private Image characterImage;
    private ImageView characterView;
    
    //private boolean canMove;
   // public Characters(){}
    
    public void setView(Image img, ImageView imgV, int numOfvpX){
        characterImage = img;
        characterView = imgV;
        imgWidth = (int) img.getWidth();
        imgHeight = (int) img.getHeight();
        viewportWidth = imgWidth/numOfvpX;
        viewportHeight = imgHeight;
        // for this game, all of the viewsports are on the first row
        viewportNum = numOfvpX;
    }
    
    public void setIsVisible(Boolean s){
        characterView.setVisible(s);
        isVisible = s;
    }
    
    public Boolean getIsVisible(){
        return isVisible;
    } 
    public void setViewport(int x,int y,int w,int h){
        characterView.setViewport(new Rectangle2D(x,y,w,h));
    }
    
    // n is the index of viewport of the image
    public void setViewportN(int n){
        if (n <= viewportNum)
        {
            characterView.setViewport(new Rectangle2D(viewportWidth*(n-1),0,viewportWidth,viewportHeight));
        }
    }


    public void setPosition(double x_c,double y_c){
        // Set the position of the character according to its centre position
        x_centre = x_c;
        y_centre =y_c;
        characterView.relocate(x_centre - (viewportWidth / 2.0), y_centre - (viewportHeight / 2.0));
    }
    
    public double getX(){
        return x_centre;
    }
    
    public double getY(){
        return y_centre;
    }
        
    
}
