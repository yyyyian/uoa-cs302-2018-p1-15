/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package characters;


import static java.lang.Math.floor;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;
import maps.Map;
import settings.Settings;

/**
 *
 * @author yianqian
 */
public class Ghosts extends Characters
{    
    // 0 for player control, 1 for AI control
    private int controlType;  
    
    //ghost1 to ghost4
    private int ghostIndex;
    
    private Girl girl;
    private Player myplayer;
    private Boolean isMoving;
    private Map wallMap;
    
    private double move_speed;
    private double slow_down_speed;
    
    private double cur_x; // x,y location in px
    private double cur_y;
    private double next_x;
    private double next_y;
    private long last_ns;
    private Boolean getKilled;
    private long last_ns_get_killed;
    
    //viewport 1, 2 represent the down view
    //viewport 3, 4 represent the up view
    //viewport 5, 6 represent the leftview
    //viewport 7, 8 represent the rightview
    private int viewportStart = 1;
    private double viewportPeriod;
    
    // Transfer the map to a map in which 
    // true represents wall cell
    // false represents acessible cell
    private Boolean[][] wall_boolean_map;   
    
    private int cur_x_coord;        // x,y location coord
    private int cur_y_coord;
    private int next_x_coord;
    private int next_y_coord;
   
    int mapX; // Col
    int mapY; // Row
    
    private int my_dir;
    private final int[] ddx = {1, 0, -1, 0};
    private final int[] ddy = {0, -1, 0, 1};

//  *************parameters to initialize*************************
    
    public Ghosts(int id)
    {
        this.ghostIndex = id;
        this.viewportPeriod = Settings.ghost_viewportPeriod;
        this.move_speed = Settings.ghost_speed;
        this.getKilled = false;
        this.my_dir = 0;
    }
    
    public void setControlType(int c)
    {
        this.controlType = c;
    }
    
    public void setPlayer(Player player){
        myplayer = player;
    }
    
    public void setGirl(Girl girl){
        this.girl = girl;
        wallMap = this.girl.getwallMap();
        mapX = wallMap.getmapCol();
        mapY = wallMap.getmapRow();
        wall_boolean_map = new Boolean[mapY][mapX];
        //set up boolean map
        for(int i = 0; i < mapX;i++)
        {
            for(int j = 0; j < mapY;j++)
            {
                wall_boolean_map[j][i] = wallMap.isWall(i, j);
            }
        }
        switch (ghostIndex){
            case 1:
                cur_x_coord = this.wallMap.getghost1BirthCoordX();
                cur_y_coord = this.wallMap.getghost1BirthCoordY();
                break;
            case 2:
                cur_x_coord = this.wallMap.getghost2BirthCoordX();
                cur_y_coord = this.wallMap.getghost2BirthCoordY();
                break;
            case 3:
                cur_x_coord = this.wallMap.getghost3BirthCoordX();
                cur_y_coord = this.wallMap.getghost3BirthCoordY();
                break;
            case 4:
                cur_x_coord = this.wallMap.getghost4BirthCoordX();
                cur_y_coord = this.wallMap.getghost4BirthCoordY();
                break;
        }
    }
    
    public void SetMoveSpeed(double s)
        {
        if (s <= 0.0)
            move_speed = 500000000.0; //0.5S
        else
        {    
            move_speed = s;           //define time in ns to move to new position
        }    
        isMoving=false;   
    }
    
    
    public void MoveRel(int n_x_coord, int n_y_coord,long cur_ns){
        cur_x = this.girl.getwallMap().mapEle_px_c_x(cur_x_coord,cur_y_coord);
        cur_y = this.girl.getwallMap().mapEle_px_c_y(cur_x_coord, cur_y_coord);
        next_x = this.girl.getwallMap().mapEle_px_c_x(n_x_coord, n_y_coord);
        next_y = this.girl.getwallMap().mapEle_px_c_y(n_x_coord, n_y_coord);
        last_ns=cur_ns;
        isMoving=true;
        cur_x_coord = n_x_coord;
        cur_y_coord = n_y_coord;
    }
    public Boolean Move(long cur_ns){
        if (cur_ns-last_ns>=move_speed){
          /*
            if (cur_x_coord == this.girl.getcur_x_coord() && 
                    cur_y_coord == this.girl.getcur_y_coord() &&
                    (!this.girl.getCanKill()) && (!this.girl.getisEatable()))
            {
                this.girl.lossOneLife();
                System.out.println("lives:"+girl.getLivesLeft());
                this.girl.setisEatable(false);
            }
            else if (cur_x_coord == this.girl.getcur_x_coord() && 
                    cur_y_coord == this.girl.getcur_y_coord() &&
                    (this.girl.getCanKill()))
            {
                getKilled = true;
                this.setPosition(this.wallMap.mapEle_px_c_x(birth_x_coord,birth_y_coord),this.wallMap.mapEle_px_c_y(birth_x_coord,birth_y_coord));
                last_ns_get_killed = cur_ns;
            }
           */ 
            setPosition(next_x,next_y);
            isMoving=false;
        }
          
        else
        {
            /*
            if(cur_ns - last_ns_get_killed > Settings.ghost_get_killed_period){
                getKilled = false;
            }*/

            double offsetX = next_x - cur_x;
            double offsety = next_y - cur_y;
            float stepPercentage = (float) ((cur_ns-last_ns)/move_speed);
            setPosition(cur_x + offsetX * stepPercentage, cur_y + offsety * stepPercentage);
            this.setViewportN(viewportStart + ((int)floor((cur_ns-last_ns)/viewportPeriod) % 2));
            isMoving=true;
        }
        return isMoving;    
    }
    
    public Map getwallMap()
    {
        return girl.getwallMap();
    }
    
    public int getcur_x_coord()
    {
        return cur_x_coord;
    }
    
    public int getcur_y_coord()
    {
        return cur_y_coord;
    }
    
    public Player getPlayer()
    {
        return myplayer;
    }
   
    public Boolean Process(long cur_ns, int PMX, int PMY){
        if((myplayer==null) && (controlType == 0))
            return false;
        
        if(isMoving){
            Move(cur_ns);
        }    
        else{
            if(controlType == 0)
            {
                if(myplayer.GetLt_Pressed() && !(this.wallMap.isWall(cur_x_coord-1, cur_y_coord))){
                    this.MoveRel(cur_x_coord-1, cur_y_coord, cur_ns);
                    viewportStart = 5; 
                }    
                else if(myplayer.GetRt_Pressed()&& !(this.wallMap.isWall(cur_x_coord+1, cur_y_coord))){
                    this.MoveRel(cur_x_coord+1, cur_y_coord, cur_ns);
                    viewportStart = 7;
                }    
                else if(myplayer.GetUp_Pressed()&& !(this.wallMap.isWall(cur_x_coord, cur_y_coord-1))){
                    this.MoveRel(cur_x_coord, cur_y_coord - 1, cur_ns);
                    viewportStart = 3;
                }    
                else if(myplayer.GetDn_Pressed()&& !(this.wallMap.isWall(cur_x_coord, cur_y_coord+1))){
                    this.MoveRel(cur_x_coord, cur_y_coord + 1, cur_ns);
                    viewportStart = 1;
                }   
                this.setViewportN(viewportStart + ((int)floor((cur_ns-last_ns)/viewportPeriod) % 2));
            }
            else if(controlType==1)
            {    //Stupid Ghost (int)(Math.random()*5)
            	if(isMoving)
            	{
            		Move(cur_ns);
                }
                else{
                    if(my_dir < 4)
                    {
                    	next_x_coord = cur_x_coord + ddx[my_dir];
                    	next_y_coord = cur_y_coord + ddy[my_dir];
                             
                        if(wall_boolean_map[next_x_coord][next_y_coord])
                        {
                        	my_dir = (my_dir + 2) % 4;
                        }
                        else
                        {
                        	MoveRel(next_x_coord , next_y_coord, cur_ns);
                        }
                        if(next_x_coord == PMX)
                        {
                        	int i;
                            for(i=1 ; (next_y_coord - i) >= 0;i++)
                            {
                            	if(wall_boolean_map[next_x_coord][next_y_coord - i]) 
                            		break;
                                if(PMY == next_y_coord - i) 
                                { 
                                	my_dir = 3;break; 
                                }
                             }
                             for(i=1;(next_y_coord + i) < wallMap.getmapRow();i++)
                             {
                             	if(wall_boolean_map[next_x_coord][next_y_coord + i]) 
                                	break;
                             	if(PMY==next_y_coord + i) 
                                { 
                             		my_dir=1;
                             		break; 
                             	}
                             }
                        }
                        else if(next_y_coord == PMY)
                        {
                        	int i;
                            for(i=1;(next_x_coord - i)>=0;i++)
                            {
                            	if(wall_boolean_map[next_x_coord - i][next_y_coord]) 
                            		break;
                                if(PMX == next_x_coord - i) 
                                { 
                                	my_dir=2;
                                	break; 
                                }
                            }
                            for(i=1;(next_x_coord + i)< wallMap.getmapCol();i++)
                            {
                            	if(wall_boolean_map[next_x_coord + i][next_y_coord]) 
                                	break;
                            	if(PMX==next_x_coord + i) 
                                { 
                            		my_dir=0;
                            		break; 
                            	}
                           }
                        }
                        this.setViewportN(viewportStart + ((int)floor((cur_ns-last_ns)/viewportPeriod) % 2));
                    }
                    return true;
                	}
            }
      }
        return false;
    }
}


