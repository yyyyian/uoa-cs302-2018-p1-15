/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package characters;

/**
 *
 * @author yianqian
 */
public class Player {
    //PLAYER1,PLAYER2,PLAYER3,AI;
    
    private boolean PLeft,PRight,PUp,PDown;
    
    public Player(){
        PLeft = false;
        PRight = false;
        PUp = false;
        PDown = false;
    }
    
    public void SetLt_Pressed(){
        PLeft=true;
        PRight=false;
        PUp=false;
        PDown=false;
    }
    
    public void SetRt_Pressed(){
        PLeft=false;
        PRight=true;
        PUp=false;
        PDown=false;
    }

    public void SetUp_Pressed(){
        PLeft=false;
        PRight=false;
        PUp=true;
        PDown=false;
     }
 
    public void SetDn_Pressed(){
        PLeft=false;
        PRight=false;
        PUp=false;
        PDown=true;
     }
    
    public boolean GetLt_Pressed(){
        return PLeft;
    }
    
    public boolean GetRt_Pressed(){
        return PRight;
    }
    
    public boolean GetUp_Pressed(){
        return PUp;
    }
    
    public boolean GetDn_Pressed(){
        return PDown;
    }
    
}
