/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package characters;

import static java.lang.Math.floor;
import javafx.scene.canvas.Canvas;
import maps.Map;
import settings.Settings;


/**
 *
 * @author yianqian
 */
public class Girl extends Characters{
    
    private Map wallMap; // this map is uneditable for AI detect //static
    private Map pelletMap; // this map is editable according to the Girl action
    private Player myplayer;
    private Boolean isMoving;
    //0.5S by default
    private double move_speed; 
    private double cur_x; // x,y coord
    private double cur_y;
    private double next_x;
    private double next_y;
    private long last_ns; 
    private long last_ns_slow_down;
    private long last_ns_can_kill;
    private long last_ns_get_killed;
    private Ghosts G1;
    private Ghosts G2;
    private Ghosts G3;
    private Ghosts G4;
    
    
//
    private int viewportStart = 1;
    //viewport 1, 2 represent the down view
    //viewport 3, 4 represent the up view
    //viewport 5, 6 represent the leftview
    //viewport 7, 8 represent the rightview
    private final double viewportPeriod;
    
    private int cur_x_coord;
    private int cur_y_coord;
    //private int next_x_coord;
    //private int next_y_coord;
    
    private int keyCollected;
    private int livesLeft;
    private boolean canKill;
    private boolean canSlowDown;

    // this is added so that it is easier for the girl
    // in case the girl is killed by the ghost right after reborning
    private boolean isEatable; 
    
    private Canvas canvastoEdit;
    //private GraphicsContext 
    
    

    public Girl() 
    {
        this.viewportPeriod = Settings.girl_viewportPeriod;
        this.move_speed = Settings.girl_speed;
        this.keyCollected = 0;
        this.livesLeft = 3;
        this.canKill = false;
        this.canSlowDown = false;
        this.isEatable = true;
        this.last_ns_can_kill = 0;
        this.last_ns_slow_down = 0;  
    }
    
    public void getGhosts(Ghosts g1,Ghosts g2, Ghosts g3, Ghosts g4){
        this.G1 = g1;
        this.G2 = g2;
        this.G3 = g3;
        this.G4 = g4;
    }
    
    public void setMap(Map uneditbleMap, Map editbleMap,Canvas canvas)
    {
        wallMap = uneditbleMap;
        pelletMap = editbleMap;
        canvastoEdit = canvas;
        cur_x_coord = uneditbleMap.getgirlBirthCoordX();
        cur_y_coord = uneditbleMap.getgirlBirthCoordY();
    }
    
    public int getcur_x_coord()
    {
        return cur_x_coord;
    }
    
    public int getcur_y_coord()
    {
        return cur_y_coord;
    }
    
    public void SetPlayer(Player p){
        myplayer = p;
    }
    
    public Player GetPlayer(){
        return myplayer;
    }
    
    public Boolean getIsMoving(){
        return isMoving;
    }
    
    public void SetMoveSpeed(double s){
        if (s <= 0.0)
            move_speed = 500000000.0; //0.5S
        else
        {    
            move_speed = s;           //define time in ns to move to new position
        }
        isMoving=false;
        
    }
    
    public void MoveRel(int n_x_coord, int n_y_coord,long cur_ns){
        cur_x = this.getwallMap().mapEle_px_c_x(cur_x_coord,cur_y_coord);
        cur_y = this.getwallMap().mapEle_px_c_y(cur_x_coord, cur_y_coord);
        next_x = this.getwallMap().mapEle_px_c_x(n_x_coord, n_y_coord);
        next_y = this.getwallMap().mapEle_px_c_y(n_x_coord, n_y_coord);
        last_ns=cur_ns;
        isMoving=true;
        cur_x_coord = n_x_coord;
        cur_y_coord = n_y_coord;
    }

    public Boolean Move(long cur_ns){
        if (cur_ns-last_ns>=move_speed){
          
            if (this.getpelletMap().isKeyPiece(cur_x_coord, cur_y_coord))
            {
                pelletMap.clearMap();
                pelletMap.setMapElement(cur_x_coord, cur_y_coord, 0);
                pelletMap.drawMap();
                keyCollected++;
                System.out.println(keyCollected);
            }
            else if (this.getpelletMap().isSlowDownGhost(cur_x_coord, cur_y_coord))
            {
                pelletMap.clearMap();
                pelletMap.setMapElement(cur_x_coord, cur_y_coord, 0);
                pelletMap.drawMap();
                canSlowDown = true;
                System.out.println(canSlowDown);
                last_ns_slow_down = cur_ns;
            }
            else if (this.getpelletMap().isKillGhost(cur_x_coord, cur_y_coord))
            {
                pelletMap.clearMap();
                pelletMap.setMapElement(cur_x_coord, cur_y_coord, 0);
                pelletMap.drawMap();
                canKill = true;
                last_ns_can_kill = cur_ns;
            }
            if (meetGhost()&& (!this.getCanKill()) && this.getisEatable())
            {
                this.lossOneLife();
                System.out.println("lives:"+ getLivesLeft());
                this.isEatable = false;
                //this.setisEatable(false);
                last_ns_get_killed = cur_ns;
            }
            
            setPosition(next_x,next_y);
            isMoving=false;
        }
        else{
            if(cur_ns - last_ns_slow_down > Settings.slow_down_period){
                this.canSlowDown = false;
                //System.out.println(canSlowDown);
            }
            if(cur_ns - last_ns_slow_down > Settings.can_kill_period){
                this.canKill = false;
            }
            if(cur_ns - last_ns_get_killed > Settings.reborn_uneatable_period){
                this.isEatable=true;
            }
            double offsetX = next_x - cur_x;
            double offsety = next_y - cur_y;
            float stepPercentage = (float) ((cur_ns-last_ns)/move_speed);
            setPosition(cur_x + offsetX * stepPercentage, cur_y + offsety * stepPercentage);
            this.setViewportN(viewportStart + ((int)floor((cur_ns-last_ns)/viewportPeriod) % 2));
            isMoving=true;

        }
        return isMoving;    
    }
    
    
    private int getG1_X_Coord(){
        return G1.getcur_x_coord();
    }
    private int getG1_Y_Coord(){
        return G1.getcur_y_coord();
    }
    private int getG2_X_Coord(){
        return G2.getcur_x_coord();
    }
    private int getG2_Y_Coord(){
        return G2.getcur_y_coord();
    }
    private int getG3_X_Coord(){
        return G3.getcur_x_coord();
    }
    private int getG3_Y_Coord(){
        return G3.getcur_y_coord();
    }
    private int getG4_X_Coord(){
        return G4.getcur_x_coord();
    }
    private int getG4_Y_Coord(){
        return G4.getcur_y_coord();
    }
    
    private Boolean meetG1(){
        return (getcur_x_coord() == this.getG1_X_Coord() && getcur_y_coord() == this.getG1_Y_Coord());
    }
    
    private Boolean meetG2(){
        return (getcur_x_coord() == this.getG2_X_Coord() && getcur_y_coord() == this.getG2_Y_Coord());
    }
    
    private Boolean meetG3(){
        return (getcur_x_coord() == this.getG3_X_Coord() && getcur_y_coord() == this.getG3_Y_Coord());
    }
    
    private Boolean meetG4(){
        return (getcur_x_coord() == this.getG4_X_Coord() && getcur_y_coord() == this.getG4_Y_Coord());
    }
    
    private Boolean meetGhost(){
        return (meetG1() || meetG2()|| meetG3() || meetG4());
    }
    
    
    
    
    public void setisEatable(Boolean bool)
    {
        isEatable = bool;
    }
    
    public boolean getCanSlowDown()
    {
        return canSlowDown;
    }
    
    public boolean getCanKill()
    {
        return canKill;
    }
    
    public int getKeyCollected()
    {
        return keyCollected;
    }
    
    public Boolean getisEatable()
    {
        return isEatable;
    }
    
    public Map getwallMap()
    {
        return wallMap;
    }
    
    public Map getpelletMap()
    {
        return pelletMap;
    }
    
    public Boolean Process(long cur_ns)
    {
        if(myplayer==null) 
        {
            return false;
        }
        if(isMoving)
        {
            Move(cur_ns);
        }    
        else{
            
            if(myplayer.GetLt_Pressed() && !(this.wallMap.isWall(cur_x_coord-1, cur_y_coord)))
            {
                this.MoveRel(cur_x_coord-1, cur_y_coord, cur_ns);
                    viewportStart = 5; 
            }    
            else if(myplayer.GetRt_Pressed()&& !(this.wallMap.isWall(cur_x_coord+1, cur_y_coord))){
                
                this.MoveRel(cur_x_coord+1, cur_y_coord, cur_ns);
                viewportStart = 7;
            }    
            else if(myplayer.GetUp_Pressed()&& !(this.wallMap.isWall(cur_x_coord, cur_y_coord-1))){
                
                this.MoveRel(cur_x_coord, cur_y_coord - 1, cur_ns);
                
                viewportStart = 3;
                
            }    
            else if(myplayer.GetDn_Pressed()&& !(this.wallMap.isWall(cur_x_coord, cur_y_coord+1))){
                
                this.MoveRel(cur_x_coord, cur_y_coord + 1, cur_ns);
                
                viewportStart = 1;
                
            }    
        }
        return true;
    }
    
    public void lossOneLife(){
        livesLeft--;
    }
    
    public int getLivesLeft(){
        return livesLeft;
    }
        
}
