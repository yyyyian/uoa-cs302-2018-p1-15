/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package maps;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import settings.Settings;


/**
 *
 * @author yianqian
 */
public class Map 
{

    
    private int mapCol;
    private int mapRow;
    private int[][] mapArray;
    private int girlBirthCoordX;
    private int girlBirthCoordY;
    private int ghost1BirthCoordX;
    private int ghost1BirthCoordY;
    private int ghost2BirthCoordX;
    private int ghost2BirthCoordY;
    private int ghost3BirthCoordX;
    private int ghost3BirthCoordY;
    private int ghost4BirthCoordX;
    private int ghost4BirthCoordY;
    private GraphicsContext map_gc;
    private int numOfKeyInit;
    private int exitCoordX;
    private int exitCoordY;
    
    public Map(){
        
    }
    /*  
    0 :     empty
    1 :     wall    top view
    2 :     wall    front view
    3 :     wall    middle front view
    4 :     wall    left end view
    5 :     wall    right end view
    6 :     pellet  keypieces
    7 :     pellet  kill ghost
    8 :     pellet  slow down ghost
    9 :     transfer spot 1
    10:     transfer spot 2
    11:     girl birth point
    12:     ghost1 birth point
    13:     ghost2 birth point
    14:     ghost3 birth point
    15:     ghost4 birth point
    16:     girl exit
    Others can be added for aesthetic feature or function
    */
    
    public void clearMap(){
        map_gc.clearRect(0, 0, 1200, 800);
    }
    
    public void drawMap() {
        //GraphicsContext map_gc = canvasOn.getGraphicsContext2D();
        //map_gc.setFill(Color.BLACK);
        //map_gc.fillRect(0, 0, 515, 453);
        int cs = Settings.cellPixel;
        Image w1 = new Image("img/wall1.png");
        Image w2 = new Image("img/wall2.png");
        Image w3 = new Image("img/wall3.png");
        Image wl = new Image("img/wallL.png");
        Image wr = new Image("img/wallR.png");
        Image kp = new Image("img/kp.png");
        Image kg = new Image("img/kg.png");
        Image sd = new Image("img/sd.png");
        //Image ts1 = new Image("");
        //Image ts2 = new Image("");
        //Image gbp = new Image("");
        //Image gbp1 = new Image("");
        //Image gbp2 = new Image("");
        //Image gbp3 = new Image("");
        //Image gbp4 = new Image("");
        Image ge = new Image("img/ge.png");
                
        for (int i = 0; i < this.getmapCol(); i++)
        {
            for(int j = 0; j < this.getmapRow(); j++)
            {
                switch (this.getMapElement(i, j))
                {
                    case 1:
                        this.get_gc().drawImage(w1, i * cs, j * cs , cs , cs);
                        break;
                    case 2:
                        this.get_gc().drawImage(w2, i * cs, j * cs , cs , cs);
                        break;
                    case 3:
                        this.get_gc().drawImage(w3, i * cs, j * cs , cs , cs);
                        break;
                    case 4:
                        this.get_gc().drawImage(wl, i * cs, j * cs , cs , cs);
                        break;
                    case 5:
                        this.get_gc().drawImage(wr, i * cs, j * cs , cs , cs);
                        break;
                    case 6:
                        this.get_gc().drawImage(kp, i * cs, j * cs , cs , cs);
                        break;
                    case 7:
                        this.get_gc().drawImage(kg, i * cs, j * cs , cs , cs);
                        break;
                    case 8:
                        this.get_gc().drawImage(sd, i * cs, j * cs , cs , cs);
                        break;
                    case 9:
                        this.get_gc().drawImage(w1, i * cs, j * cs , cs , cs);
                        break;
                    case 10:
                        this.get_gc().drawImage(w1, i * cs, j * cs , cs , cs);
                        break;
                    case 11:
                        this.get_gc().drawImage(w1, i * cs, j * cs , cs , cs);
                        break;
                    case 12:
                        this.get_gc().drawImage(w1, i * cs, j * cs , cs , cs);
                        break;
                    case 13:
                        this.get_gc().drawImage(w1, i * cs, j * cs , cs , cs);
                        break;
                    case 14:
                        this.get_gc().drawImage(w1, i * cs, j * cs , cs , cs);
                        break;
                    case 15:
                        this.get_gc().drawImage(w1, i * cs, j * cs , cs , cs);
                        break;
                    case 16:
                        this.get_gc().drawImage(ge, i * cs, j * cs , cs , cs);
                        break;
                }
            }
                    
                        //if(!Settings.mymap.getMapElement(i,j).getIsVisible()){
                    //map_gc.setFill(Color.BLACK);
                    
                    //ImageView iv = new ImageView (w1);
                    //map_gc.drawImage(w1, i*100, j*100, 100, 100);
                    //map_gc.fillRect(i*100, j*100, 100, 100);
                    
                    
        }
    }
    
    //public void drawWall1(){
    //    
    //}
    
    public GraphicsContext get_gc()
    {
        return map_gc;
    }
    
    
    public void setMap(int x, int y, int[][] marray,Canvas canvasOn){
        mapCol = x;
        mapRow = y;
        mapArray = new int[y][x];
        map_gc = canvasOn.getGraphicsContext2D();
        for(int i = 0 ; i < y; i++)
        {
            for(int j = 0 ; j < x; j++)
            {
                mapArray[i][j] = marray[i][j];
                if (mapArray[i][j] == 6)
                {
                numOfKeyInit++;
                }
                else if (mapArray[i][j] == 11)
                {
                girlBirthCoordX = j;
                girlBirthCoordY = i;
                }
                else if (mapArray[i][j] == 12)
                {
                ghost1BirthCoordX = j;
                ghost1BirthCoordY = i;
                }
                else if (mapArray[i][j] == 13)
                {
                ghost2BirthCoordX = j;
                ghost2BirthCoordY = i;
                }
                else if (mapArray[i][j] == 14)
                {
                ghost3BirthCoordX = j;
                ghost3BirthCoordY = i;
                }
                else if (mapArray[i][j] == 15)
                {
                ghost4BirthCoordX = j;
                ghost4BirthCoordY = i;
                }
                else if (mapArray[i][j] == 16)
                {
                exitCoordX = j;
                exitCoordY = i;
                }
                
                /*switch (mapArray[i][j]) {
                    case 6:
                        numOfKeyInit++;
                        break;
                    case 11:
                        girlBirthCoordX = j;
                        girlBirthCoordY = i;
                        break;
                    case 12:
                        ghost1BirthCoordX = j;
                        ghost1BirthCoordY = i;
                        break;
                    case 13:
                        ghost2BirthCoordX = j;
                        ghost2BirthCoordY = i;
                        break;
                    case 14:
                        ghost3BirthCoordX = j;
                        ghost3BirthCoordY = i;
                        break;
                    case 15:
                        ghost4BirthCoordX = j;
                        ghost4BirthCoordY = i;
                        break;
                    case 16:
                        exitCoordX = j;
                        exitCoordY = i;
                        break;
                    default:
                        break;
                }*/
            }
        }
    }
    
    public int getNumOfKeyInit(){
        return numOfKeyInit;
    }
    
    public int[][] getMapArray()
    {
        return mapArray;
    }
    
    //Users input: (x,y)coord, return the element value; 
    public int getMapElement(int x, int y){
        return mapArray[y][x];
    }
    
    public void setMapElement(int x, int y, int elementValue){
        mapArray[y][x] = elementValue;
    }
    
    public Boolean isWall(int x, int y)
    {
        return (mapArray[y][x] > 0) && (mapArray[y][x] < 6);
    }
    
    public Boolean isKeyPiece(int x, int y)
    {
        return mapArray[y][x] == 6;
    }
    
    public Boolean isKillGhost(int x, int y)
    {
        return mapArray[y][x] == 7;
    }
    
    public Boolean isSlowDownGhost(int x, int y)
    {
        return mapArray[y][x] == 8;
    }
    
    public Boolean isTransferSpot1(int x, int y)
    {
        return mapArray[y][x] == 9;
    }
    
    public Boolean isTransferSpot2(int x, int y)
    {
        return mapArray[y][x] == 10;
    }
    
    public Boolean isExit(int x, int y)
    {
        return mapArray[y][x] == 16;
    }
    
    public int getgirlBirthCoordX()
    {
        return girlBirthCoordX;
    }
    public int getgirlBirthCoordY()
    {
        return girlBirthCoordY;
    }
    
    public int getghost1BirthCoordX()
    {
        return ghost1BirthCoordX;
    }
    public int getghost1BirthCoordY()
    {
        return ghost1BirthCoordY;
    }
    
    public int getghost2BirthCoordX()
    {
        return ghost2BirthCoordX;
    }
    
    public int getghost2BirthCoordY()
    {
        return ghost2BirthCoordY;
    }
    
    public int getghost3BirthCoordX()
    {
        return ghost3BirthCoordX;
    }
    
    public int getghost3BirthCoordY()
    {
        return ghost3BirthCoordY;
    }
    
    public int getghost4BirthCoordX()
    {
        return ghost4BirthCoordX;
    }
    
    public int getghost4BirthCoordY()
    {
        return ghost4BirthCoordY;
    }
    
    public int getExitCoordX()
    {
        return exitCoordX;
    }
    
    public int getExitCoordY()
    {
        return exitCoordY;
    }
    
    public int getmapCol()
    {
        return mapCol;
    }
    public int getmapRow()
    {
        return mapRow;
    }
    
    


    public double mapEle_px_c_x(int x, int y){
        return (Settings.ls_canvas_x + (x + 0.5) * Settings.map_ele_length); 
    }
    
    
    public double mapEle_px_c_y(int x, int y){
        return (Settings.ls_canvas_y + (y + 0.5) * Settings.map_ele_length); 
    }
    
}
