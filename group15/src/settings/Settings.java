/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package settings;

import characters.Player;
import java.util.ArrayList;
import java.util.Arrays;
//import maps.MapView;

/**
 *
 * @author yianqian
 */
public class Settings {

    /**
     *
     */
    //public static MapView mymap;
    public static Boolean player2Enabled;
    public static Boolean player3Enabled;

    public static Player Player1;
    public static Player Player2;
    public static Player Player3;
    
    public static ArrayList<String> GhostplayerList= new ArrayList<>(Arrays.asList("AI","AI","AI","AI"));
    public static Boolean level_complete;
    public static Boolean winningState;
    //public static Boolean lossingState;
    
    public static int cellPixel = 40;
    public static int numOfRow = 20;
    public static int numOfCol = 30;
    
    // the left top coord of the map canvas 
    public static double ls_canvas_x = 50.0;
    public static double ls_canvas_y = 50.0;
    
    // the character girl size
    public static double girl_width = 40.0;
    public static double girl_height = 40.0;
    
    // the map elements length
    public static double map_ele_length = 40.0;
    
    // the time limit of a level
    public static int level_time = 120; // for testing, using 20 s
    public static double level_timer_width = 100.0;
    public static double level_timer_height = 480.0;
     /*  
    0 :     empty
    1 :     wall    top view
    2 :     wall    front view
    3 :     wall    middle front view
    4 :     wall    left end view
    5 :     wall    right end view
    6 :     pellet  keypieces
    7 :     pellet  kill ghost
    8 :     pellet  slow down ghost
    9 :     transfer spot 1
    10:     transfer spot 2
    11:     girl birth point
    12:     ghost1 birth point
    13:     ghost2 birth point
    14:     ghost3 birth point
    15:     ghost4 birth point
    16:     girl exit
    Others can be added for aesthetic feature or function
    */
    
    
                                           //                              1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 
                                           //0 ,1 ,2 ,3 ,4 ,5 ,6 ,7 ,8 ,9 ,0 ,1 ,2 ,3 ,4 ,5 ,6 ,7 ,8 ,9 ,0 ,1 ,2 ,3 ,4 ,5 ,6 ,7 ,8 ,9
    public static int[][] mapElementsArray={{1 ,2 ,1 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,1 },//0
                                            {1 ,11,1 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,1 },//1
                                            {1 ,0 ,1 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,7 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,1 },//2
                                            {1 ,0 ,1 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,1 },//3
                                            {1 ,0 ,1 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,1 },//4
                                            {1 ,0 ,1 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,8 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,1 },//5
                                            {1 ,0 ,4 ,3 ,3 ,0 ,0 ,0 ,0 ,0 ,14,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,1 },//6
                                            {1 ,0 ,6 ,6 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,1 },//7
                                            {1 ,0 ,6 ,0 ,0 ,0 ,0 ,0 ,13,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,1 },//8
                                            {1 ,0 ,6 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,1 },//9
                                            {1 ,0 ,6 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,1 },//10
                                            {1 ,0 ,6 ,0 ,0 ,0 ,0 ,12,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,1 },//11
                                            {1 ,0 ,6 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,1 },//12
                                            {1 ,0 ,6 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,1 },//13
                                            {1 ,0 ,6 ,0 ,0 ,0 ,0 ,0 ,0 ,7 ,0 ,0 ,0 ,8 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,1 },//14
                                            {1 ,0 ,6 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,1 },//15
                                            {1 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,15,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,1 },//16
                                            {1 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,1 },//17
                                            {1 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,16,1 },//18
                                            {4 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,5 }};//19
    
    //public static double numOfkeyPieces = 10;
    
    
    public static double girl_speed = 200000000.0; //0.2s
    public static double ghost_speed = 200000000.0; //0.2s
    //public static double ghost1_speed = 200000000.0; //0.2s
    //public static double ghost2_speed = 200000000.0; //0.2s
    //public static double ghost3_speed = 200000000.0; //0.2s
    //public static double ghost4_speed = 200000000.0; //0.2s
    
    public static double girl_viewportPeriod = 400000000.0; // 0.5s
    public static double ghost_viewportPeriod = 400000000.0; //0.5s
    
    public static double slow_down_period = 10000000000.0; //10 s
    public static double can_kill_period = 10000000000.0; //10 s
    public static double reborn_uneatable_period = 5000000000.0; // 5 s
    public static double ghost_get_killed_period = 3000000000.0; //3 s
    
    
    // winning and lossing scripts:
    public static String winningScript = "-YOU ESCAPE THE SORCERESS'S CASTLE-";
    public static String lossingScript = "-YOU FAILED TO ESCAPE-";
    
    
}
