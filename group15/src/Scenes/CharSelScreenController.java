/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Scenes;


import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.ResourceBundle;
import javafx.animation.AnimationTimer;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.paint.Color;
import static javafx.scene.paint.Color.BLACK;
import static javafx.scene.paint.Color.RED;
import javafx.scene.text.Text;
import settings.Settings;

/**
 * FXML Controller class
 *
 * @author yianqian
 */
public class CharSelScreenController implements Initializable {

    /**
     * Initializes the controller class.
     */
    private int curstate;
    private Main application;
    private Scene cur_scene;
    private ArrayList<String> css_GhostPlayerList;
    @FXML
    Text css_G1_player;
    @FXML
    Text css_G2_player;
    @FXML
    Text css_G3_player;
    @FXML
    Text css_G4_player;
    @FXML
    Text css_start;
    @FXML
    Text css_back;
    @FXML
    private Text css_title;
    @FXML
    private Text css_G1;
    @FXML
    private Text css_G2;
    @FXML
    private Text css_G3;
    @FXML
    private Text css_G4;
    @FXML
    private ImageView css_charImg;
    @FXML
    private Text css_charDescri;
    
    private AnimationTimer AT1;
    private long last_ns_1;
    
    public void setApp(Main application){
        this.application = application;
    }
    
    public void setScene(Scene s){
        this.cur_scene = s;
    }
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        curstate = 0;
        css_GhostPlayerList = new ArrayList<>(Arrays.asList("AI","AI","AI","AI"));;
        /*
        curstate    0   anchor over css_G1_Player
        curstate    1   anchor over css_G2_Player
        curstate    2   anchor over css_G3_Player
        curstate    3   anchor over css_G4_Player
        curstate    4   anchor over css_start
        curstate    5   anchor over css_back
        */
        //this is done to detect the anchor
        css_G1_player.setFill(Color.web("#6a8d92"));
        css_G1.setFill(Color.web("#6a8d92"));
        css_G2_player.setFill(Color.web("#f7bfb4"));
        css_G2.setFill(Color.web("#f7bfb4"));
        css_G3_player.setFill(Color.web("#f7bfb4"));
        css_G3.setFill(Color.web("#f7bfb4"));
        css_G4_player.setFill(Color.web("#f7bfb4"));
        css_G4.setFill(Color.web("#f7bfb4"));
        css_start.setFill(Color.web("#f7bfb4"));
        css_back.setFill(Color.web("#f7bfb4"));
        /*Ghost1_player = "AI";
        Ghost2_player = "AI";
        Ghost3_player = "AI";
        Ghost4_player = "AI";
        */ 
        AT1 = new AnimationTimer()
                {
                    @Override
                    public void handle(long cur_ns_1)
                    {
                        mytimer1(cur_ns_1); 
                    }
                };

        last_ns_1 = System.nanoTime();
        AT1.start();
    }    
    
    private void mytimer1(long cur_ns_1) 
    {
           if(cur_ns_1 - last_ns_1 >=100000000.0) {
                cur_scene.setOnKeyPressed(new EventHandler<KeyEvent>(){
                    @Override
                    public void handle(KeyEvent event) {
                        KeyPress(event);
                    }
                });
               last_ns_1 = cur_ns_1;
           }
    }
   
    
    private String returnPlayer(int ghostNo){
        String player = "AI";
            if((!css_GhostPlayerList.contains("PLAYER2")) && (!css_GhostPlayerList.contains("PLAYER3")))
            {
                player = "PLAYER2";
                css_GhostPlayerList.set(ghostNo,"PLAYER2");
            }
            else if((css_GhostPlayerList.contains("PLAYER2")) && (!css_GhostPlayerList.contains("PLAYER3")))
            {
                    player = "PLAYER3";
                    css_GhostPlayerList.set(ghostNo,"PLAYER3");
            }
            else if((!css_GhostPlayerList.contains("PLAYER2")) && (css_GhostPlayerList.contains("PLAYER3")))
            {
                if(css_GhostPlayerList.get(ghostNo).equals("PLAYER3"))
                {
                    player = "AI";
                    css_GhostPlayerList.set(ghostNo,"AI");
                }
                else
                {
                    player = "PLAYER2";
                    css_GhostPlayerList.set(ghostNo,"PLAYER2");
                }
            }
            else if((css_GhostPlayerList.contains("PLAYER2")) && (css_GhostPlayerList.contains("PLAYER3")))
            {
                if(!css_GhostPlayerList.get(ghostNo).equals("AI"))
                {
                    player = "AI";
                    css_GhostPlayerList.set(ghostNo,"AI");
                }
            }
            
        return player;
    }
    
    public void KeyPress(KeyEvent event){
        if ((event.getCode() == KeyCode.LEFT ||event.getCode() == KeyCode.RIGHT) && curstate==0) 
        {
            css_G1_player.setText(returnPlayer(curstate));
          
        }
        else if((event.getCode() == KeyCode.DOWN) && curstate==0) 
        {
            css_G2_player.setFill(Color.web("#6a8d92"));
            css_G2.setFill(Color.web("#6a8d92"));
            css_G1_player.setFill(Color.web("#f7bfb4"));
            css_G1.setFill(Color.web("#f7bfb4"));
            curstate = 1;
        }
        else if ((event.getCode() == KeyCode.LEFT ||event.getCode() == KeyCode.RIGHT) && curstate==1) 
        {
          css_G2_player.setText(returnPlayer(curstate));
          
        }
        else if((event.getCode() == KeyCode.UP) && curstate==1) 
        {
            curstate = 0;
            css_G1_player.setFill(Color.web("#6a8d92"));
            css_G1.setFill(Color.web("#6a8d92"));
            css_G2_player.setFill(Color.web("#f7bfb4"));
            css_G2.setFill(Color.web("#f7bfb4"));
        }
        else if((event.getCode() == KeyCode.DOWN) && curstate==1) 
        {
            curstate = 2;
            css_G3_player.setFill(Color.web("#6a8d92"));
            css_G3.setFill(Color.web("#6a8d92"));
            css_G2_player.setFill(Color.web("#f7bfb4"));
            css_G2.setFill(Color.web("#f7bfb4"));
        }
        else if ((event.getCode() == KeyCode.LEFT ||event.getCode() == KeyCode.RIGHT) && curstate==2) 
        {
          css_G3_player.setText(returnPlayer(curstate));
          
        }
        else if((event.getCode() == KeyCode.UP) && curstate==2) 
        {
            curstate = 1;
            css_G2_player.setFill(Color.web("#6a8d92"));
            css_G2.setFill(Color.web("#6a8d92"));
            css_G3_player.setFill(Color.web("#f7bfb4"));
            css_G3.setFill(Color.web("#f7bfb4"));
        }
        else if((event.getCode() == KeyCode.DOWN) && curstate==2) 
        {
            curstate = 3;
            css_G4_player.setFill(Color.web("#6a8d92"));
            css_G4.setFill(Color.web("#6a8d92"));
            css_G3_player.setFill(Color.web("#f7bfb4"));
            css_G3.setFill(Color.web("#f7bfb4"));
        }
        else if ((event.getCode() == KeyCode.LEFT ||event.getCode() == KeyCode.RIGHT) && curstate==3) 
        {
            css_G4_player.setText(returnPlayer(curstate));
        }
        else if((event.getCode() == KeyCode.UP) && curstate==3) 
        {
          curstate = 2;
            css_G3_player.setFill(Color.web("#6a8d92"));
            css_G3.setFill(Color.web("#6a8d92"));
            css_G4_player.setFill(Color.web("#f7bfb4"));
            css_G4.setFill(Color.web("#f7bfb4"));
        }
        else if((event.getCode() == KeyCode.DOWN) && curstate==3) 
        {
            curstate = 4;
            css_start.setFill(Color.web("#6a8d92"));
            css_G4_player.setFill(Color.web("#f7bfb4"));
            css_G4.setFill(Color.web("#f7bfb4"));
        }
        else if((event.getCode() == KeyCode.RIGHT) && curstate==4) 
        {
            curstate = 5;
            css_back.setFill(Color.web("#6a8d92"));
            css_start.setFill(Color.web("#f7bfb4"));
        }
        else if((event.getCode() == KeyCode.UP) && curstate==4) 
        {
            curstate = 3;
            css_start.setFill(Color.web("#f7bfb4"));
            css_G4_player.setFill(Color.web("#6a8d92"));
            css_G4.setFill(Color.web("#6a8d92"));
        }
        else if((event.getCode() == KeyCode.ENTER) && curstate==4) 
        {
            Settings.GhostplayerList = css_GhostPlayerList;
            System.out.println(Settings.GhostplayerList);
            // transfer the game mode to settings
            AT1.stop();
            application.gotoLevelScreen();
        }
        else if((event.getCode() == KeyCode.ENTER) && curstate==5) 
        {
            AT1.stop();
            application.gotoWelcomlScreen();
        }
        else if((event.getCode() == KeyCode.LEFT) && curstate==5) 
        {
            curstate = 4;
            css_start.setFill(Color.web("#6a8d92"));
            css_back.setFill(Color.web("#f7bfb4"));
        }

}
}

