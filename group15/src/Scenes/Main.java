/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Scenes;

import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.fxml.JavaFXBuilderFactory;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.WindowEvent;
/**
 *
 * @author yianqian
 */

public class Main extends Application {

    private Stage stage;
    
    private final double MINIMUM_WINDOW_WIDTH = 1440.0;
    private final double MINIMUM_WINDOW_HEIGHT = 900.0;
    
   public static void main(String[] args) {
        Application.launch(Main.class, (java.lang.String[])null);
    }

    @Override
    public void start(Stage primaryStage) {
        try {
            stage = primaryStage;
            stage.setTitle("A Little Adventure in a Sorceress’s Castle");
            stage.setMinWidth(MINIMUM_WINDOW_WIDTH);
            stage.setMinHeight(MINIMUM_WINDOW_HEIGHT);
            gotoWelcomlScreen();
            
            primaryStage.show();
            //gotoCharSelScreen();
        } catch (Exception ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private Initializable replaceSceneContent(String fxml) throws Exception {
        FXMLLoader loader = new FXMLLoader();
        InputStream in = Main.class.getResourceAsStream(fxml);
        loader.setBuilderFactory(new JavaFXBuilderFactory());
        loader.setLocation(Main.class.getResource(fxml));
        AnchorPane page;
        try {
            page = (AnchorPane) loader.load(in);
        } finally {
            in.close();
        } 
        Scene scene = new Scene(page, 1440, 900);
        stage.setScene(scene);
        stage.sizeToScene();   
        return (Initializable) loader.getController();
    }

    public void gotoWelcomlScreen() {
        try {
            //stage.getScene().setOnKeyPressed(null);
            WelcomeScreenController WS = (WelcomeScreenController) replaceSceneContent("WelcomeScreen.fxml");
            WS.setApp(this);
            WS.setScene(stage.getScene());
        } catch (Exception ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void gotoCharSelScreen() {
        try {
            CharSelScreenController CSS = (CharSelScreenController) replaceSceneContent("CharSelScreen.fxml");
            CSS.setApp(this);
            CSS.setScene(stage.getScene());
        } catch (Exception ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void gotoLevelScreen() {
        try {
            //System.out.println("new controller");
            LevelScreenController LS = (LevelScreenController) replaceSceneContent("LevelScreen.fxml");
            System.out.println("SetApp");
            LS.setApp(this);
            LS.setScene(stage.getScene());
        } catch (Exception ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void gotoResultScreen() {
        try {
            ResultScreenController RS = (ResultScreenController) replaceSceneContent("ResultScreen.fxml");
            RS.setApp(this);
            RS.setScene(stage.getScene());
        } catch (Exception ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public Stage getStage(){
        return stage;
    }
}