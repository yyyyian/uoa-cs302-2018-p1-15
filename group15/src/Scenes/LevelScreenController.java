/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Scenes;

import characters.GhostPlayers;
import characters.Ghosts;
import characters.Girl;
import characters.Player;
import javafx.scene.image.Image;
//import javafx.scene.image.ImageView;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.animation.AnimationTimer;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.paint.Color;
import maps.Map;
//import maps.MapView;
import settings.Settings;
//import img;

/**
 * FXML Controller class
 *
 * @author yianqian
 */
public class LevelScreenController implements Initializable {

    /**
     * Initializes the controller class.
     */
    
    private Main application;
    private Scene cur_scene;
    private Boolean countdowning;
    private Boolean game_paused;
    private Boolean level_complete;
    private AnimationTimer AT;
    private long last_ns;
    private int countdown_timer;
    private double timer_step_width;
    private GraphicsContext timer_bar;
    private Girl P1;
    private Ghosts G1;
    private Ghosts G2;
    private Ghosts G3;
    private Ghosts G4;
    private Map wallMap;
    private Map pelletMap;
    
    //private boolean girlLoss;
    
    @FXML
    private Label ls_countdown;
    @FXML
    private javafx.scene.canvas.Canvas ls_map;
    @FXML
    private javafx.scene.canvas.Canvas ls_timer;
    @FXML
    private ImageView ls_player1;
    @FXML
    private ImageView ls_ghost1;
    @FXML
    private ImageView ls_ghost2;
    @FXML
    private ImageView ls_ghost3;
    @FXML
    private ImageView ls_ghost4;
    
    //private GraphicsContext map_gc;
    
    public void setApp(Main application){
        this.application = application;
    }
    public void setScene(Scene s){
        this.cur_scene = s;
    }
    
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
       
       timer_bar = ls_timer.getGraphicsContext2D();
       timer_bar.setFill(Color.web("#826665"));
       timer_bar.fillRect(0, 0, Settings.level_timer_width, Settings.level_timer_height);
       timer_step_width = Settings.level_timer_height / Settings.level_time;
       Settings.Player1=new Player();
       Settings.Player2=new Player();
       Settings.Player3=new Player();
       
       game_paused=false;
       level_complete = false;
       countdown_timer = Settings.level_time;
       Settings.winningState = false;
       //Settings.lossingState = false;
       wallMap = new Map();
       wallMap.setMap(Settings.numOfCol,Settings.numOfRow,Settings.mapElementsArray,ls_map);
       pelletMap = new Map();
       pelletMap.setMap(Settings.numOfCol,Settings.numOfRow,Settings.mapElementsArray,ls_map);
       
       P1 = new Girl();
       P1.setMap(wallMap,pelletMap,ls_map);
       P1.setView(new Image("img/player1.png"), ls_player1,8);
       System.out.println("1");
       double sgx = wallMap.mapEle_px_c_x(P1.getcur_x_coord(),P1.getcur_y_coord());
       double sgy = wallMap.mapEle_px_c_y(P1.getcur_x_coord(),P1.getcur_y_coord());
       P1.setPosition(sgx,sgy);
       P1.setIsVisible(true);
       P1.SetPlayer(Settings.Player1);
       P1.SetMoveSpeed(Settings.girl_speed);
       P1.setViewportN(1);
     
       G1 = new Ghosts(1);
       if(Settings.GhostplayerList.get(0).equals("AI"))
       {
           G1.setControlType(1);
       }
       else if(Settings.GhostplayerList.get(0).equals("PLAYER2"))
       {
           G1.setControlType(0);
           G1.setPlayer(Settings.Player2);
       }
       else if(Settings.GhostplayerList.get(0).equals("PLAYER3"))
       {
           G1.setControlType(0);
           G1.setPlayer(Settings.Player3);
       }
       else
       {
    	   G1.setControlType(1);
       }
       G1.setGirl(P1);
       G1.setView(new Image("img/player1.png"), ls_ghost1,8);
       System.out.println("2");
       //double sg1x = wallMap.mapEle_px_c_x(G1.getcur_x_coord(),G1.getcur_y_coord());
       //double sg1y = wallMap.mapEle_px_c_y(G1.getcur_x_coord(),G1.getcur_y_coord());
       double sg1x = wallMap.mapEle_px_c_x(G1.getcur_x_coord(),G1.getcur_y_coord());
       double sg1y = wallMap.mapEle_px_c_y(G1.getcur_x_coord(),G1.getcur_y_coord());
       System.out.println(G1.getcur_x_coord()+G1.getcur_y_coord());
       G1.setPosition(sg1x,sg1y);
       G1.setIsVisible(true);
       G1.SetMoveSpeed(Settings.ghost_speed);
       G1.setViewportN(1);
       
       G2 = new Ghosts(2);
       if(Settings.GhostplayerList.get(1).equals("AI"))
       {
           G2.setControlType(1);
       }
       else if(Settings.GhostplayerList.get(1).equals("PLAYER2"))
       {
           G2.setControlType(0);
           G2.setPlayer(Settings.Player2);
       }
       else if(Settings.GhostplayerList.get(1).equals("PLAYER3"))
       {
           G2.setControlType(0);
           G2.setPlayer(Settings.Player3);
       }
       else
       {
    	   G2.setControlType(1);
       }
       G2.setGirl(P1);
       G2.setView(new Image("img/player1.png"), ls_ghost1,8);
       double sg2x = wallMap.mapEle_px_c_x(G2.getcur_x_coord(),G2.getcur_y_coord());
       double sg2y = wallMap.mapEle_px_c_y(G2.getcur_x_coord(),G2.getcur_y_coord());
       G1.setPosition(sg2x,sg2y);
       G1.setIsVisible(true);
       G1.SetMoveSpeed(Settings.ghost_speed);
       G1.setViewportN(1);
       
       G3 = new Ghosts(3);
       if(Settings.GhostplayerList.get(2).equals("AI"))
       {
           G3.setControlType(1);
           System.out.println("AI1");
       }
       else if(Settings.GhostplayerList.get(2).equals("PLAYER2"))
       {
           G3.setControlType(0);
           G3.setPlayer(Settings.Player2);
       }
       else if(Settings.GhostplayerList.get(2).equals("PLAYER3"))
       {
           G3.setControlType(0);
           G3.setPlayer(Settings.Player3);
       }
       else
       {
    	   G3.setControlType(1);
       }
       G3.setGirl(P1);
       G3.setView(new Image("img/player1.png"), ls_ghost1,8);
       double sg3x = wallMap.mapEle_px_c_x(G3.getcur_x_coord(),G3.getcur_y_coord());
       double sg3y = wallMap.mapEle_px_c_y(G3.getcur_x_coord(),G3.getcur_y_coord());
       G3.setPosition(sg3x,sg3y);
       G3.setIsVisible(true);
       G3.SetMoveSpeed(Settings.ghost_speed);
       G3.setViewportN(1);
       
       G4 = new Ghosts(4);
       if(Settings.GhostplayerList.get(3).equals("AI"))
       {
           G4.setControlType(1);
       }
       else if(Settings.GhostplayerList.get(3).equals("PLAYER2"))
       {
           G4.setControlType(0);
           G4.setPlayer(Settings.Player2);
       }
       else if(Settings.GhostplayerList.get(3).equals("PLAYER3"))
       {
           G4.setControlType(0);
           G4.setPlayer(Settings.Player3);
       }
       else
       {
    	   G4.setControlType(1);
       }
       G4.setGirl(P1);
       G4.setView(new Image("img/player1.png"), ls_ghost1,8);
       double sg4x = wallMap.mapEle_px_c_x(G4.getcur_x_coord(),G4.getcur_y_coord());
       double sg4y = wallMap.mapEle_px_c_y(G4.getcur_x_coord(),G4.getcur_y_coord());
       G4.setPosition(sg4x,sg4y);
       G4.setIsVisible(true);
       G4.SetMoveSpeed(Settings.ghost_speed);
       G4.setViewportN(1);
       
       P1.getGhosts(G1, G2, G3, G4);
       
       
       
        AT = new AnimationTimer()
                {
                    @Override
                    public void handle(long cur_ns)
                    {
                        mytimer(cur_ns); 
                    }
                };
        
        
        countdowning = true;
        //MapView.mapInit(ls_map);
        //MapView.drawMap(ls_map);
        wallMap.drawMap();
        
        last_ns = System.nanoTime();
        //ls_countdown.setText("3");
        AT.start();
        
    }
    
    private void mytimer(long cur_ns) {
        if (countdowning) {
            if(cur_ns - last_ns <= 1000000000.0){
                ls_countdown.setText("3");
            }
            else if(cur_ns- last_ns<= 2000000000.0){
                ls_countdown.setText("2");
            }
            else if(cur_ns- last_ns<= 3000000000.0){
                ls_countdown.setText("1");
                
            }
        else
            {
                countdowning = false;
                cur_scene.setOnKeyPressed(new EventHandler<KeyEvent>(){
         
                    @Override
                    public void handle(KeyEvent event) {
                        keyPress(event);
                    }
                
                });
                //gaming = TRUE;
                
                ls_countdown.setVisible(false);
                last_ns = cur_ns;
                
            }        
        }
        else if (game_paused){
            ls_countdown.setText("Press Y to resume, Press N to Exit to Main");
            ls_countdown.setVisible(true);
        }
        else{
        if (level_complete)
        {
            AT.stop();
            application.gotoResultScreen();
        }
        else if(P1.getKeyCollected() == wallMap.getNumOfKeyInit() && 
                    P1.getcur_x_coord() == wallMap.getExitCoordX() &&
                    P1.getcur_y_coord() == wallMap.getExitCoordY()){
                Settings.winningState = true;
                level_complete = true;
                System.out.println(Settings.winningState);
            }
        else if(P1.getLivesLeft() < 0){
                level_complete = true;
            }
        else if(countdown_timer <= 0){
                level_complete = true;
            }    
        else{   
             
            P1.Process(cur_ns);
            //G1.Process(cur_ns,P1.getcur_x_coord(),P1.getcur_y_coord());
            //G2.Process(cur_ns,P1.getcur_x_coord(),P1.getcur_y_coord());
            //G3.Process(cur_ns,P1.getcur_x_coord(),P1.getcur_y_coord());
            //G4.Process(cur_ns,P1.getcur_x_coord(),P1.getcur_y_coord());
           if(cur_ns - last_ns > 1000000000.0) {
               playctdn();
               last_ns = cur_ns;
           }
         }   
        }
    }

    private void playctdn()
    {
        
        countdown_timer--;
        timer_bar.clearRect(0, 0, Settings.level_timer_width, Settings.level_timer_height);
        timer_bar.fillRect(0, Settings.level_timer_height - timer_step_width * countdown_timer, Settings.level_timer_width, timer_step_width * countdown_timer);
        
    }

    public void keyPress(KeyEvent event){
        
        if(game_paused){
            if (event.getCode() == KeyCode.Y){
                game_paused = false;
                ls_countdown.setVisible(false);
            }
            else if(event.getCode() == KeyCode.N){
                AT.stop();
                game_paused = false;
                application.gotoWelcomlScreen();
            }
        }
        else{
            if (event.getCode() == KeyCode.P){
                game_paused = true;
            }
            else if (event.getCode() == KeyCode.R){
                //application.winningState = TRUE;
                application.gotoResultScreen();
            }
            else if (event.getCode() == KeyCode.UP){
                Settings.Player1.SetUp_Pressed();
            }
            else if (event.getCode() == KeyCode.DOWN){
                Settings.Player1.SetDn_Pressed();
            }
            else if (event.getCode() == KeyCode.LEFT){
                Settings.Player1.SetLt_Pressed();
            }
            else if (event.getCode() == KeyCode.RIGHT){
                Settings.Player1.SetRt_Pressed();
            }
            else if(event.getCode() == KeyCode.W){
                Settings.Player2.SetUp_Pressed();
            }
            else if(event.getCode() == KeyCode.S){
                Settings.Player2.SetDn_Pressed();
            }
            else if(event.getCode() == KeyCode.A){
                Settings.Player2.SetLt_Pressed();
            }
            else if(event.getCode() == KeyCode.D){
                Settings.Player2.SetRt_Pressed();
            }
            else if(event.getCode() == KeyCode.I){
                Settings.Player3.SetUp_Pressed();
            }
            else if(event.getCode() == KeyCode.K){
                Settings.Player3.SetDn_Pressed();
            }
            else if(event.getCode() == KeyCode.J){
                Settings.Player3.SetLt_Pressed();
            }
            else if(event.getCode() == KeyCode.L){
                Settings.Player3.SetRt_Pressed();
            }
           
            
        }
    }

    
}