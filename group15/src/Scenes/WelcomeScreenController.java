/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Scenes;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.animation.AnimationTimer;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;

/**
 * FXML Controller class
 *
 * @author yianqian
 */
public class WelcomeScreenController extends AnchorPane implements Initializable {

    /**
     * Initializes the controller class.
     */
    private int curstate;
    private Scene cur_scene;
    private Boolean exit_on;
    @FXML
    Text ws_exit;
    private Main application;
    @FXML
    private VBox ws_vbox;
    @FXML
    private Text ws_newgame;
    @FXML
    private ImageView ws_logo;
    @FXML
    private Rectangle ws_exitConfirm;
    @FXML
    private Text ws_exitMessage;
    
    
    
    
    public void setApp(Main application){
        this.application = application;
    }
    
    public void setScene(Scene s){
        this.cur_scene = s;
    }
    
    private AnimationTimer AT1;
    private long last_ns_1;
    private long last_ns_2;
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) 
    { 
        curstate = 0;
        exit_on = false;
        ws_exit.setFill(Color.web("#826665"));
        ws_newgame.setFill(Color.web("#f5dbda"));
        ws_exitConfirm.setVisible(false);
        ws_exitMessage.setVisible(false);
        AT1 = new AnimationTimer()
                {
                    @Override
                    public void handle(long cur_ns_1)
                    {
                        mytimer1(cur_ns_1); 
                    }
                };

        last_ns_1 = System.nanoTime();
        last_ns_2 = System.nanoTime();
        AT1.start();
    }
   /*
    private void mytimer1(long cur_ns_1) 
    {
        if (exit_on){
            ws_exitConfirm.setVisible(true);
            ws_exitMessage.setVisible(true);
        }
        else
        {
           if(cur_ns_1 - last_ns_1 >= 0.0 && cur_ns_1 - last_ns_1 < 600000000.0) 
           {
                ws_newgame.setOpacity(0.8);
                ws_exit.setOpacity(0.8);
           }
           else if(cur_ns_1 - last_ns_1 >= 600000000.0 && cur_ns_1 - last_ns_1 < 1200000000.0) 
           {
                ws_newgame.setOpacity(0.6);
                ws_exit.setOpacity(0.6);
           }
           else if (cur_ns_1 - last_ns_1 >= 1200000000.0 && cur_ns_1 - last_ns_1 < 1500000000.0) 
           {
                ws_newgame.setOpacity(0.8);
                ws_exit.setOpacity(0.8);
           }
           else if (cur_ns_1 - last_ns_1 >= 1500000000.0 && cur_ns_1 - last_ns_1 < 1800000000.0) 
           {
                ws_newgame.setOpacity(1.0);
                ws_exit.setOpacity(1.0);
           }
           else if (cur_ns_1 - last_ns_1 >= 1800000000.0) 
           {
               last_ns_1 = cur_ns_1;
           }
           if(cur_ns_1 - last_ns_2 >=0.0) {
             
                cur_scene.setOnKeyPressed(new EventHandler<KeyEvent>(){
                    @Override
                    public void handle(KeyEvent event) {
                        keyboardEvent(event);
                    }
                });
               last_ns_2 = cur_ns_1;
           }
        }*/
    private void mytimer1(long cur_ns_1) 
    {
        if(cur_ns_1 - last_ns_1 >= 0.0 && cur_ns_1 - last_ns_1 < 600000000.0) 
           {
                ws_newgame.setOpacity(0.8);
                ws_exit.setOpacity(0.8);
           }
           else if(cur_ns_1 - last_ns_1 >= 600000000.0 && cur_ns_1 - last_ns_1 < 1200000000.0) 
           {
                ws_newgame.setOpacity(0.6);
                ws_exit.setOpacity(0.6);
           }
           else if (cur_ns_1 - last_ns_1 >= 1200000000.0 && cur_ns_1 - last_ns_1 < 1500000000.0) 
           {
                ws_newgame.setOpacity(0.8);
                ws_exit.setOpacity(0.8);
           }
           else if (cur_ns_1 - last_ns_1 >= 1500000000.0 && cur_ns_1 - last_ns_1 < 1800000000.0) 
           {
                ws_newgame.setOpacity(1.0);
                ws_exit.setOpacity(1.0);
           }
           else if (cur_ns_1 - last_ns_1 >= 1800000000.0) 
           {
               last_ns_1 = cur_ns_1;
           }
        if(cur_ns_1 - last_ns_2 >=0.0) {
             
                cur_scene.setOnKeyPressed(new EventHandler<KeyEvent>(){
                    @Override
                    public void handle(KeyEvent event) {
                        keyboardEvent(event);
                    }
                });
                if (exit_on){
                    ws_exitConfirm.setVisible(true);
                    ws_exitMessage.setVisible(true);
                 } 
               last_ns_2 = cur_ns_1;
           }
    }      
    private void keyboardEvent(KeyEvent event) {
        if(exit_on)
        {
            if (event.getCode() == KeyCode.Y){
               application.getStage().close();
            }
            else if(event.getCode() == KeyCode.N)
            {
                exit_on = false;
                ws_exitConfirm.setVisible(false);
                ws_exitMessage.setVisible(false);
            }
        }
        if (event.getCode() == KeyCode.ESCAPE)
        {
            exit_on = true;
        }
        else if (event.getCode() == KeyCode.DOWN && curstate==0) 
        {
          ws_newgame.setFill(Color.web("#826665"));
          ws_exit.setFill(Color.web("#f5dbda"));
          curstate=1;
        }
        else if (event.getCode() == KeyCode.UP && curstate==1) 
        {
          ws_exit.setFill(Color.web("#826665"));
          ws_newgame.setFill(Color.web("#f5dbda"));
          curstate=0;
        }
        else if (event.getCode() == KeyCode.ENTER && curstate==0) 
        {
          ws_exit.setFill(Color.web("#826665"));
          ws_newgame.setFill(Color.web("#c45c82"));
          application.gotoCharSelScreen();
        }
        else if (event.getCode() == KeyCode.ENTER && curstate==1) 
        {
          exit_on = true;
        }
    }
}


