/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Scenes;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.animation.AnimationTimer;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import settings.Settings;

/**
 * FXML Controller class
 *
 * @author yianqian
 */
public class ResultScreenController implements Initializable {

    /**
     * Initializes the controller class.
     */
    private Main application;
    private Scene cur_scene;
    @FXML
    private VBox rs_hbox;
    @FXML
    private Label rs_pressEnter;
    @FXML
    private Text rs_gameResult;
    private AnimationTimer AT1;
    private long last_ns_1;
    private long last_ns_2;
    
    
    
    
    public void setApp(Main application){
        this.application = application;
    }
    public void setScene(Scene s){
        this.cur_scene = s;
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        //curstate = 0;
        //ws_start.setTextFill(RED);
        //ws_exit.setTextFill(BLACK);
        //System.out.println("init");
        
        if(Settings.winningState)
        {
            rs_gameResult.setText(Settings.winningScript);
        }
        else
        {
            rs_gameResult.setText(Settings.lossingScript);
        }
        
        AT1 = new AnimationTimer()
                {
                    @Override
                    public void handle(long cur_ns_1)
                    {
                        mytimer1(cur_ns_1); 
                    }
                };

        last_ns_1 = System.nanoTime();
        last_ns_2 = System.nanoTime();
        //System.out.println(last_ns_1);
        AT1.start();
        //System.out.println("init s4");
        
    }
    
    private void mytimer1(long cur_ns_1) {
        
           if(cur_ns_1 - last_ns_1 >= 0.0 && cur_ns_1 - last_ns_1 < 600000000.0) {
           //    playctdn();
                 
                rs_pressEnter.setOpacity(0.8);
               //last_ns = cur_ns;
           }
           else if(cur_ns_1 - last_ns_1 >= 600000000.0 && cur_ns_1 - last_ns_1 < 1200000000.0) {
           //    playctdn();
                 
                rs_pressEnter.setOpacity(0.6);
               //last_ns = cur_ns;
           }
           else if (cur_ns_1 - last_ns_1 >= 1200000000.0 && cur_ns_1 - last_ns_1 < 1500000000.0) {
           //    playctdn();
               rs_pressEnter.setOpacity(0.8);
               //last_ns = cur_ns;
           }
           else if (cur_ns_1 - last_ns_1 >= 1500000000.0 && cur_ns_1 - last_ns_1 < 1800000000.0) {
           //    playctdn();
               rs_pressEnter.setOpacity(1.0);
               //last_ns = cur_ns;
           }
           
           else if (cur_ns_1 - last_ns_1 >= 1800000000.0) {
           //    playctdn();
               
               last_ns_1 = cur_ns_1;
           }
           if(cur_ns_1 - last_ns_2 >=1000000000.0) {
           //    playctdn();
                 
                cur_scene.setOnKeyPressed(new EventHandler<KeyEvent>(){
                    //@Override
                   //public void handler(KeyEvent event){
                    //    keyPress(event);
                    //}

                    @Override
                    public void handle(KeyEvent event) {
                        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                        pressEnter(event);
                    }
                
                });
               last_ns_2 = cur_ns_1;
           }
           
           
    
    }      
     
   public void pressEnter(KeyEvent event) 
   { 
      if (event.getCode() == KeyCode.ENTER) {
          application.gotoWelcomlScreen();
      }
    
    }
}  
